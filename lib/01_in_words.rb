class Fixnum

  def in_words

    # The string value is used to implement logic later on
    str_self= self.to_s

    # Hashed the values that are particularly tricky
    zero_to_teens = {
      0 => "zero",
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine",
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen",
    }
    tens = {
      2 => "twenty ",
      3 => "thirty ",
      4 => "forty ",
      5 => "fifty ",
      6 => "sixty ",
      7 => "seventy ",
      8 => "eighty ",
      9 => "ninety ",
    }
    case self
    when 0..19
      return zero_to_teens[self]
    when 20..99
      if self % 10 == 0
        return tens[self / 10].strip
      else
        tens[self / 10] + zero_to_teens[self % 10]
      end
    when 100..999
      if self % 100 == 0
        return zero_to_teens[self / 100] + " hundred"
      else
        return zero_to_teens[self / 100] + " hundred " + (self % 100).in_words
      end
    when 1000..999999
      if self % 1000 == 0
        return (self / 1000).in_words + " thousand"
      else
        return (self / 1000).in_words + " thousand " + (self % 1000).in_words
      end
    when 1000000..999999999
      if self % 1000000 == 0
        return (self / 1000000).in_words + " million"
      else
        return (self / 1000000).in_words + " million " + (self % 1000000).in_words
      end
    when 1000000000..999999999999
      if self % 1000000000 == 0
        return (self / 1000000000).in_words + " billion"
      else
        return (self / 1000000000).in_words + " billion " + (self % 1000000000).in_words
      end
    when 1000000000000..999999999999999
      if self % 1000000000000 == 0
        return (self / 1000000000000).in_words + " trillion"
      else
        return (self / 1000000000000).in_words + " trillion " + (self % 1000000000000).in_words
      end
    end
  end
end
